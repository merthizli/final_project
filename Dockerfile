FROM python:3

COPY . /usr/backend
WORKDIR /usr/backend

RUN pip3 install -r requirements.txt

ENV GOOGLE_APPLICATION_CREDENTIALS='./serviceaccount.json'

EXPOSE 5001

CMD ["python3", "./app.py"]