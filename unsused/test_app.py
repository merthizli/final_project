## dont forget to expose the outside port when running the app 
from flask import Flask, render_template, request, jsonify
from flask_restful import Api
from flask_cors import CORS
from werkzeug import secure_filename
# from tensorflow.contrib.tensor_forest.python import tensor_forest
# from tensorflow.python.ops import resources


#You may add additional imports
import warnings
#warnings.simplefilter("ignore")
import pandas as pd
import numpy as np
import sklearn as sk
# from sklearn import model_selection
# from sklearn import naive_bayes
# from sklearn import neural_network
# from sklearn import ensemble
# from sklearn import decomposition, pipeline
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import cross_val_score
import matplotlib.pyplot as plt
import time


import pandas as pd
import logging
#import tensorflow as tf
import numpy as np
#import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split

# For image
#from io import StringIO
#import base64


app = Flask(__name__)
cors = CORS(app)

@app.route('/')
def main():
	logging.debug("Hit the main page")
	return render_template('index.html')


def upload_blob(bucket_name, source_file_name, destination_blob_name):
    """Uploads a file to the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    print('File {} uploaded to {}.'.format(
        source_file_name,
        destination_blob_name))


def download_blob(bucket_name, source_blob_name, destination_file_name):
    """Downloads a blob from the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(source_blob_name)

    blob.download_to_filename(destination_file_name)

    print('Blob {} downloaded to {}.'.format(
        source_blob_name,
        destination_file_name))

@app.route('/uploader', methods = ['POST'])
def upload_file():
	f = request.files['file']
	f.save(secure_filename(f.filename))
	upload_blob("mertbucket", f.filename, f.filename)
	return "Ok"


@app.route('/test', methods = ['POST'])
def test():
	f = request.files['file']
	f.save(secure_filename(f.filename))
	download_blob("mertbucket", f.filename, f.filename)
	return "Ok"


def list_blobs(bucket_name):
    """Lists all the blobs in the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)

    blobs = bucket.list_blobs()

    for blob in blobs:
        print(blob.name)


@app.route('/trainmodel')
def train_model():

	data = pd.read_csv("./trainSet.csv")
	print(data.shape)
	data.head(5)

	features = data[data.columns[:-1]]
	labels =data[data.columns[-1:]]

	print("features")
	print(features.head())
	print("labels")
	print(labels)

	train_features,test_features, train_labels, test_labels = sk.model_selection.train_test_split(features,labels, test_size= 0.2)
	print(test_features)
	dtc = sk.tree.DecisionTreeClassifier(criterion='entropy')
	tree = dtc.fit(train_features, train_labels) 
	predictions = tree.predict(test_features)
	acc = sk.metrics.accuracy_score(test_labels.values.ravel(), predictions)
	print("** acc")
	print(acc)

	# data = pd.read_csv('./trainSet.csv')

	# input_x = data.iloc[:, 0:-1].values

	# input_y = data.iloc[:, -1].values


	# X_train, X_test, y_train, y_test = train_test_split(input_x, input_y, test_size = 0.25, random_state = 0)
	
	
	return render_template('index.html', plot_url=test_features)

if __name__ == '__main__':
	#logging.basicConfig(filename='/var/log/1.log',level=logging.DEBUG)
	app.run(debug=True, host='0.0.0.0')
