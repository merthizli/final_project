from googleapiclient import discovery
#from oauth2client.client import GoogleCredentials
from google.oauth2 import service_account
from google.cloud import storage


def main():
	SERVICE_ACCOUNT_FILE = 'serviceaccount.json'
	credentials = service_account.Credentials.from_service_account_file(
        SERVICE_ACCOUNT_FILE)

	#credentials = GoogleCredentials.get_application_default()
	service = discovery.build('storage', 'v1', credentials=credentials)

	filename = './trainSet.csv'
	bucket = 'mertbucket'

	body = {'name': filename[2:]}
	#req = service.objects().insert(bucket=bucket, body=body, media_body=filename)
	#resp = req.execute() 
	req = service.objects().get(bucket=bucket, object="dest_file_name.csv")
	resp = req.execute() 
	print (resp)


def main2():
	bucket_name= 'mertbucket'
	source_blob_name = "dest_file_name.csv"
	destination_file_name= "ahmet.csv"

	storage_client = storage.Client()
	bucket = storage_client.get_bucket(bucket_name)
	blob = bucket.blob(source_blob_name)

	blob.download_to_filename(destination_file_name)

	print('Blob {} downloaded to {}.'.format(
	    source_blob_name,
	    destination_file_name))


def upload_blob(bucket_name, source_file_name, destination_blob_name):
    """Uploads a file to the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    print('File {} uploaded to {}.'.format(
        source_file_name,
        destination_blob_name))


def download_blob(bucket_name, source_blob_name, destination_file_name):
    """Downloads a blob from the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(source_blob_name)

    blob.download_to_filename(destination_file_name)

    print('Blob {} downloaded to {}.'.format(
        source_blob_name,
        destination_file_name))
    

if __name__ == '__main__':
	main2()

